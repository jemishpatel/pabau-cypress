const loginPage = require("../page objects/login.page");
const selectLocationPage = require("../page objects/select.location.page");
const dashboardPage = require("../page objects/dashboard.page");

describe('Demo Smoke Test', function() {
   beforeEach(function() {
       cy.visit('/');
   });

    it('C1 User should be able to login into system', function() {
        cy.fixture('testUsers.json').then(user => {

            // Verify "Login Page" is displayed
            loginPage.verifyFirstLoginPageIsDisplayed();

            loginPage.fillFirstLoginForm(user.email);

            loginPage.verifySecondLoginPageIsDisplayed();

            loginPage.fillSecondLoginForm(user.email, user.password);

            selectLocationPage.verifySelectLocationPageDisplayed();

            selectLocationPage.selectCompanyLocation(user.company);

            dashboardPage.verifyDashboardPageIsDisplayed(user.username);
        });
    });

    it('C2 Test failure demo', function() {
        expect(1).eq(1);
    });
});