const ELEMENTS = {
    LOCATION_SELECTION_CONTAINER_CSS: '#all_contacts',

    /**
     * This button return dynamic company selection button
     * @param companyName
     * @returns {string}
     */
    COMPANY_SELECT_BUTTON_XPATH: (companyName) => {
        return `.//*[@id="all_contacts"]//*[contains(text(),"${companyName}")]/../..//button`;
    }
};

class SelectLocationPage{

    verifySelectLocationPageDisplayed(){
        cy.get(ELEMENTS.LOCATION_SELECTION_CONTAINER_CSS).should("be.visible");
    }

    selectCompanyLocation(companyName){
        this.clickOnSelectCompanyButton(companyName);
    }

    clickOnSelectCompanyButton(companyName){
        cy.xpath(ELEMENTS.COMPANY_SELECT_BUTTON_XPATH(companyName)).click();
    }
}

module.exports = new SelectLocationPage();