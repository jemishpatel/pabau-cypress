const ELEMENTS = {
    FIRST_LOGIN_FORM_CONTAINER_CSS: '.panel form, .login_details',
    EMAIL_INPUT_CSS: 'input[name=email]',
    PASSWORD_INPUT_CSS: 'input[name=password]',
    SIGN_IN_BUTTON_CSS: 'button#sign-in-button',
    LOADER_CONTAINER_CSS: 'div.loader-overlay',
    SECOND_LOGIN_FORM_CONTAINER_CSS: 'div.page-login-main'
};

class LoginPage{

    // ---------------------- verify elements methods -------------------------- //
    verifyFirstLoginPageIsDisplayed(){
        cy.get(ELEMENTS.FIRST_LOGIN_FORM_CONTAINER_CSS).should("be.visible");
    }

    verifySecondLoginPageIsDisplayed(){
        cy.get(ELEMENTS.SECOND_LOGIN_FORM_CONTAINER_CSS).should("be.visible");
    }

    // ---------------------- form filling methods --------------------------------- //
    fillFirstLoginForm(email){
        cy.get(ELEMENTS.EMAIL_INPUT_CSS).type(email);
        this.clickOnNextButton();
    }

    fillSecondLoginForm(email, password){
        cy.get(ELEMENTS.EMAIL_INPUT_CSS).should("have.value",email);
        cy.get(ELEMENTS.PASSWORD_INPUT_CSS).type(password);
        this.clickOnSignInButton();
    }

    // ---------------------- clicking methods -------------------------------------- //
    clickOnNextButton(){
        this.clickOnSignInButton();
    }

    clickOnSignInButton(){
        cy.get(ELEMENTS.SIGN_IN_BUTTON_CSS).click();
    }
}


module.exports = new LoginPage();