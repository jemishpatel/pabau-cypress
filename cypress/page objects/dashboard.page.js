const ELEMENTS = {
    LOGGED_USER_STATUS_LINK_XPATH: './/ul[@id="status-infos"]/li[text()="Logged in as: "]//a'
};


class DashboardPage{
    // ---------------------- verify elements methods -------------------------- //
    verifyDashboardPageIsDisplayed(username){
        cy.xpath(ELEMENTS.LOGGED_USER_STATUS_LINK_XPATH).should('be.visible').contains(username);
    }
}

module.exports = new DashboardPage();